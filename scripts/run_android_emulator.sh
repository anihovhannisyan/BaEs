#This script runs already installed android emulator on you device.

#cd ~/Android/Sdk/tools/bin && ./avdmanager list avd
#cd $ANDROID_HOME/tools/bin; ./emulator -avd Nexus_5X_API_23

#!/bin/bash
 
ANDROID_HOME=$ANDROID_HOME
AVD_NAME=Nexus_5X_API_23
 
if [ $# -gt 0 ]; then
    AVD_NAME=$1
    shift
fi

pushd ${ANDROID_HOME}/tools
./emulator -avd ${AVD_NAME} $* &
popd
