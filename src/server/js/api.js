// This file contains all API specific callbacks and all requests handlings.
// This file represent API structure that is referred from server.js file.

API = {};

// AUTHENTICATION REQUESTS PART
API['authCallback'] = function (req, res) {
  //TODO: Handle authentication via google/phone
  res.send('Authenticated.');
};

API['authValidCallback'] = function (req, res) {
  res.send('authValidCallback.');
};

// VIEWING REQUESTS SECTION
API['accountCallback'] = function (req, res) {
  res.send('account.');
};
API['toursCallback'] = function (req, res) {
  res.send('tours.');
};
API['eventsCallback'] = function (req, res) {
  res.send('events.');
};
API['ordersCallback'] = function (req, res) {
  res.send('orders.');
};
API['goodsCallback'] = function (req, res) {
  res.send('goods.');
};

// ADMINISTRATIVE REQUESTS SECTION
// tours that should be updated by admins,
// see the docs/server_api.js -> AVAILABLE APIS -> "1." point

API['toursUpdateCallback'] = function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  console.log('Got POST request: ');
  console.log(req.body);
  //Timeout for slow network connection - 1s
  setTimeout(function () {
    //TODO: move to callback function
    res.send(JSON.stringify({
      firstName: req.body.title || null,
      lastName: req.body.price || null
    }));
  }, 1000);
};

// Helper function
// TODO: move here tours callback function
/////////////////////////////////////////////
module.exports = API;
