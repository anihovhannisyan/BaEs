// This file contains start point of BaEs server side.
// This file represent refers to libraries and api.js for API handlings.

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();

api = require('./api.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
//TODO: Understand and describe why is this raw
app.use(express.static(path.join(__dirname, 'www')));

app.listen(3000, () => console.log('Example app listening on port 3000!'))

// SERVER SIDE AVAILABLE APIS
app.get('/', (req, res) => res.send('Hello from BaEs!'))

// AUTHENTICATION REQUESTS PART
//TODO: Handle authentication
app.get('/auth', api.authCallback)
app.post('/auth_valid', api.authValidCallback)

// VIEWING REQUESTS SECTION
app.get('/account', api.accountCallback)

app.get('/tours', api.toursCallback)

app.get('/events', api.eventsCallback)

app.get('/orders', api.ordersCallback)

app.get('/goods', api.goodsCallback)

// ADMINISTRATIV REQUESTS SECTION
app.post('/tours/update', api.toursUpdateCallback)
